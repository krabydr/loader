let scope = $('#Shar_1')[0].children.length + 1,
    i = 0,
    str = '.path',
    time = () => 20000

$('.str').css({
    "animation-duration": time() / scope + "ms"
})
const startCounter = () => {
    let promise = new Promise(function(resolve, reject) {
        $(str + i).show();
        resolve();
    });
    return promise
}

let interval = setInterval(() => {
    startCounter()
        .then(() => {
            $(str + (i - 1)).hide();
            i++;
            if (i === scope) {
                $(str + (scope - 1)).hide();
                i = 0;
            }
        })
}, time() / scope);

setTimeout(() => {
    clearInterval(interval);
    $('.loadred').remove();
}, time() + (time() / scope))